import torch

def calculate_accuracy(loader, model, local_rank):
    '''com acc'''
    model.eval()
    correct = 0
    total = 0
    with torch.no_grad():
        for data, target in loader:
            # 将数据和标签移动到当前 GPU
            data, target = data.to(local_rank), target.to(local_rank)
            # 模型预测
            output = model(data)
            _, predicted = torch.max(output, 1)
            # 统计正确数量和总数量
            correct += (predicted == target).sum().item()
            total += target.size(0)
    return correct / total
