import torch
import torch.nn as nn
import torchvision.models as models


class MNISTModel(nn.Module):
    def __init__(self):
        super(MNISTModel, self).__init__()
        # 卷积层
        self.conv1 = nn.Conv2d(1, 32, kernel_size=3, padding=1)  # (32x28x28)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, padding=1)  # (64x28x28)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)  # (64x14x14)
        self.dropout = nn.Dropout(0.5)  # Dropout
        # 全连接层
        self.fc1 = nn.Linear(64 * 14 * 14, 128)
        self.fc2 = nn.Linear(128, 10)  # 10 分类

    def forward(self, x):
        x = torch.relu(self.conv1(x))
        x = torch.relu(self.conv2(x))
        x = self.pool(x)
        x = self.dropout(x)
        x = x.view(x.size(0), -1)  # 展平
        x = torch.relu(self.fc1(x))
        x = self.fc2(x)
        return x


def get_model(data_name: str = 'MNIST'):
    if data_name == 'MNIST':
        mnist_model = MNISTModel()
        return mnist_model
    elif data_name == 'CIFAR10':
        resnet50 = models.resnet50(pretrained=False)
        resnet50.fc = nn.Linear(resnet50.fc.in_features, 10)
        return resnet50
